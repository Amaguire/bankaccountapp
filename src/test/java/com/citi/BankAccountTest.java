package com.citi;

import static org.junit.Assert.*;

import org.junit.Test;

public class BankAccountTest {

	
	@Test
	public void test_Deposit_CorrectBalance() {
		
		BankAccount bankAccount = new BankAccount("AnnaMaguire");
		double balance = bankAccount.deposit(100);
		
		assertTrue("Balance Check", balance == 100);
		System.out.println("Balance correct");
		
	}

	@Test
	public void test_Deposit_IncorrectBalance() {
		
		BankAccount bankAccount = new BankAccount("Anna");
		double balance = bankAccount.deposit(100);
		
		assertFalse("Balance Check", balance == 1);
		
		
	}
	@Test
	public void test_Withdraw_CorrectBalance() {
		
		BankAccount bankAccount = new BankAccount("Anna");
		bankAccount.deposit(100);
		double balance = bankAccount.withdraw(60);
		
		assertTrue("Balance Check", balance== 40);
		
}
}
